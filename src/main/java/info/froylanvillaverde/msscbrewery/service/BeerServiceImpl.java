package info.froylanvillaverde.msscbrewery.service;

import info.froylanvillaverde.msscbrewery.web.model.BeerDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class BeerServiceImpl implements BeerService {

    @Override
    public BeerDto getBeerById(UUID beerId) {
        return BeerDto.builder()
                .id(UUID.randomUUID())
                .beerName("Galaxy cat")
                .beerStyle("Pale ale")
                .build();
    }

    @Override
    public BeerDto saveNewBeer(BeerDto beerDto) {
        log.info("Beer saved [{}]", beerDto);
        return BeerDto.builder()
                .id(UUID.randomUUID())
                .build();
    }

    @Override
    public void updateBeer(UUID beerId, BeerDto beerDto) {
        log.info("Beer updated with id [{}] and body [{}]", beerId, beerDto);
    }

    @Override
    public void deleteBeerById(UUID beerId) {
        log.info("Beer deleted with id [{}]", beerId);
    }
}
