package info.froylanvillaverde.msscbrewery.service;

import info.froylanvillaverde.msscbrewery.web.model.CustomerDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService{

    @Override
    public CustomerDto getCustomerById(UUID customerId) {
        return CustomerDto.builder()
                .id(UUID.randomUUID())
                .name("Froy")
                .build();
    }

    @Override
    public CustomerDto saveNewCustomer(CustomerDto customerDto) {
        log.info(customerDto.toString());
        return CustomerDto.builder()
                .id(UUID.randomUUID())
                .build();
    }

    @Override
    public void updateCustomer(UUID customerId, CustomerDto customerDto) {
        log.info("Customer updated with id [{}] and body [{}]", customerId, customerDto);
    }

    @Override
    public void deleteCustomerById(UUID customerId) {
        log.info("Customer deleted with id [{}]", customerId);
    }
}
